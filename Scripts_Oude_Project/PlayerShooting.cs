﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    RaycastHit RayHit;
    private float TheDamage = 25f;
    private float m_Projectilespeed = 75f;

    [SerializeField]
    private ParticleSystem m_ParticleShoot;

    [SerializeField]
    private Rigidbody m_Projectile;

    [SerializeField]
    private Transform OriginPlayer;

    void Start()
    {
        m_ParticleShoot.gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ShootProjectile();
            ShootRaycast();
            Debug.DrawLine(OriginPlayer.position, OriginPlayer.forward, Color.red);
            m_ParticleShoot.gameObject.SetActive(true);
            m_ParticleShoot.Play();
        }
    }

    private void ShootRaycast()
    {
        if (Physics.Raycast(OriginPlayer.position, -OriginPlayer.forward * 100f, out RayHit, 100f))
        {
            if (RayHit.transform.CompareTag("Player"))
            {
                Debug.Log(RayHit);
                RayHit.transform.gameObject.GetComponent<HealthPlayers>().DoDamage(TheDamage);
                RayHit.transform.gameObject.GetComponent<SliderScript>().Player2Damage();                
            }
            if (RayHit.transform.CompareTag("Explosive"))
            {
                RayHit.transform.gameObject.GetComponent<AOD>().DealDamage(25f);
            }
        }
    }
    private void ShootProjectile()
    {
        Rigidbody instantiatedProjectile = Instantiate(m_Projectile, transform.position, transform.rotation) as Rigidbody;
        instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(0, 0, -m_Projectilespeed));
    }
}

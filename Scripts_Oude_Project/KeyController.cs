﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyController : MonoBehaviour
{

    [SerializeField]
    private GameObject _KeyPickup;

    [SerializeField]
    private GameObject m_Checkbox;

    //[SerializeField]
    //private GameObject m_KeyNotification;

    [SerializeField]
    private GameObject m_KeyIcon;

    private ItemHolder _ItemHolder;

    private Animator m_Animator;

    private bool _KeyPickedup;

    void Start()
    {
        GameObject Player = GameObject.Find("Player");
        _ItemHolder = Player.GetComponent<ItemHolder>();
        //m_Animator = m_KeyNotification.GetComponent<Animator>();
    }

    void Update()
    {
        if (_KeyPickedup)
        {
            _ItemHolder._HasKey = true;
            m_KeyIcon.SetActive(true);
            Destroy(_KeyPickup);

            // Activate notification
            //m_KeyNotification.SetActive(true);
            //m_Animator.SetBool("KeyPickedUp", true);

        }
    }

    public void StopNotification()
    {
        //m_KeyNotification.SetActive(false);
    }

    public void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Player"))
        {
            _KeyPickedup = true;
            m_Checkbox.SetActive(true);

        }


    }
}

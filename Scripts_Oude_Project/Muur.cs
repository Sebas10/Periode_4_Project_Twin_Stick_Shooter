﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muur : MonoBehaviour {

    public Vector3 Speed;
    public float OpenSpeed = 3;
    public float TimeToOpen = 3;

    public IEnumerator Open()
    {
        float CurrentTime = 0;
        Debug.Log(TimeToOpen);
        while (CurrentTime <= TimeToOpen)
        {
            CurrentTime += Time.deltaTime;
            transform.Translate(Speed * Time.deltaTime);
            yield return null;
            Debug.Log("T");
        }

        yield return null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public void OnButtonClickStartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void OnButtonClickOptions()
    {
        SceneManager.LoadScene("Options");
    }

    public void OnButtonClickQuitGame()
    {
        Application.Quit();
    }
    public void OnButtonClickControls()
    {
        SceneManager.LoadScene("Controls");
    }


}

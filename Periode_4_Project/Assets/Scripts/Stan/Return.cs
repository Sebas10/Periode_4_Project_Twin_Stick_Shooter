﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Return : MonoBehaviour
{

    [SerializeField] private int m_CurrentLevel;


    public void OnButtonClickMainMenu()
    {
        SceneManager.LoadScene(0);
    }
    
    public void OnButtonClickRestart()
    {
        SceneManager.LoadScene(m_CurrentLevel);
    }

}

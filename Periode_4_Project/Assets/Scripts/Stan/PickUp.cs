﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public bool KeyCardPickUp = false;
    public bool KeyCardPickedUp = false;
	void Start ()
    {
		
	}

	void Update ()
    {
		if(KeyCardPickUp && Input.GetKeyDown(KeyCode.E))
        {
            KeyCardPickedUp = true;
            Destroy(gameObject);

        }
	}

    private void OnTriggerEnter(Collider other)
    {
        KeyCardPickUp = true;
    }
    private void OnTriggerExit(Collider other)
    {
        KeyCardPickUp = false;
    }
}

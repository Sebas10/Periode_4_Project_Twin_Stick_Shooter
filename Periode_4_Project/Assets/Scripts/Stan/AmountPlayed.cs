﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmountPlayed : MonoBehaviour
{
    [SerializeField] protected Text Played;
    [SerializeField] private int TimesPlayed = 0;


    private void Awake()
    {
        TimesPlayed += 1;
    }
    void Update ()
    {
        PlayerPrefs.SetInt("Times", TimesPlayed);
        Played.text = TimesPlayed.ToString();
	}
}

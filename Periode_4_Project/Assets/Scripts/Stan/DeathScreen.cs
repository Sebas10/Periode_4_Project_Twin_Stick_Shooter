﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathScreen : AmountPlayed
{
    private int m_TimesPlayed = 0;

    [SerializeField] private GameObject m_Deathscreen;

    [SerializeField] private Slider m_Slider_PlayerHP;

    [SerializeField] private PauseGame m_Paused;

    void Start()
    {
        m_Deathscreen.SetActive(false);
        Played.gameObject.SetActive(false);
    }

    void Update()
    {
        if (m_Slider_PlayerHP.value <= 0)
        {
            PlayerDeath();
        }

        m_TimesPlayed = PlayerPrefs.GetInt("Times");

        Played.text = m_TimesPlayed.ToString();
    }

    public void PlayerDeath()
    {
        m_Paused.m_PauseB = true;
        Played.gameObject.SetActive(true);
        m_Deathscreen.SetActive(true);
    }
}

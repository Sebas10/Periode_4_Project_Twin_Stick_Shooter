﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    [SerializeField] Transform Door;
    [SerializeField] GameObject Player;
    [SerializeField] GameObject Keycard;
    [SerializeField] private bool Collision;
    private bool AllowedClick;
    [SerializeField] private PickUp pickup;

   //[SerializeField] private GameObject m_Lights;

    [SerializeField] private float speed;

    [SerializeField] private Transform m_Target;

    public bool Move = false;

    private void Start()
    {
        //if (m_Lights != null)
        //{
        //    m_Lights.SetActive(false);
        //}
        Collision = false;
        AllowedClick = true;
    }

    void Update()
    {
        float step = speed * Time.deltaTime;


        if (pickup.KeyCardPickedUp)
        {
            if (Input.GetKeyDown(KeyCode.F) && Collision && AllowedClick)
            {                
                Move = true;
                AllowedClick = false;
            }
        }

        if (Move)
        {
            //if(m_Lights != null)
            //{
            //    m_Lights.SetActive(true);
            //}
            transform.position = Vector3.Lerp(transform.position, m_Target.position, step);
            if(transform.position.y <= -5)
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter(Collider Player)
    {
        Collision = true;
    }

    private void OnTriggerExit(Collider Player)
    {
        Collision = false;
    }
}

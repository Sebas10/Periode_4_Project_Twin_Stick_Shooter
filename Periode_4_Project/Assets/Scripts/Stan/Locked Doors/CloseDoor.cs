﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseDoor : MonoBehaviour
{
    [SerializeField] GameObject Door;
    [SerializeField] GameObject Player;
    [SerializeField] private bool Collision;
    [SerializeField] GameObject Keycard;
    [SerializeField] private PickUp pickup;
    private bool AllowedClick;

    private void Start()
    {
        Collision = false;
        AllowedClick = true;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && Collision && AllowedClick)
        {
            if (Input.GetKeyDown(KeyCode.F) && Collision && AllowedClick)
            {
                Door.GetComponent<Animation>().Play("CloseDoor");
                AllowedClick = false;
            }
            pickup.KeyCardPickedUp = false;
        }
        
    }

    private void OnTriggerEnter(Collider Player)
    {
        Collision = true;
    }

    private void OnTriggerExit(Collider Player)
    {
        Collision = false;
    }
}

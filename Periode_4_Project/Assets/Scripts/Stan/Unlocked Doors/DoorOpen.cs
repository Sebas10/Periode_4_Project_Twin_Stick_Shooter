﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour
{
    [SerializeField] GameObject Door;
    [SerializeField] GameObject Player;
    [SerializeField] private bool Collision;
    [SerializeField] Transform EndPos;
    

    private float OpenSpeed = 5;

    private void Start()
    {
        Collision = false;
    }
    void Update()
    {
        if (Collision)
        {
            transform.position = Vector3.Lerp(transform.position , EndPos.position, OpenSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider Player)
    {
        Collision = true;
    }

    private void OnTriggerExit(Collider Player)
    {
        Collision = false;
    }
}

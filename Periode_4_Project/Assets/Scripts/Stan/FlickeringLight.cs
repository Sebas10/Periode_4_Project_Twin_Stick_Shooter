﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class FlickeringLight : MonoBehaviour {

    //gemaakt met behulp van een tutorial
    public delegate void MainLoop();

    public event MainLoop m_MainLoop;

    public float scale;
    public float speed;

    [HideInInspector]public bool MakeSourceStationary;
    [HideInInspector]public float positionOffset;

    private Light m_light;

    private float intensityOrigin;
    private float intensityOffset;
    private float intensityDelta;
    private float rangeOrigin;
    private float rangeOffset;
    private float rangeTarget;
    private float rangeDelta;

    private Vector3 positionOrigin;
    private Vector3 positionDelta;

    private bool setNewTargets;

    private float deltaSum = 0;


    void Start ()
    {
        m_light = GetComponent<Light>();

        intensityOrigin = m_light.intensity;
        rangeOrigin = m_light.range;
        positionOrigin = transform.position;

        setNewTargets = true;

        scale *= 0.1f;
        speed *= 0.02f;

        intensityOffset = m_light.intensity * scale;
        rangeOffset = m_light.range * scale;
        positionOffset *= scale * 0.1f;

        m_MainLoop += IntensityAndRange;

        if (!MakeSourceStationary)
        {
            m_MainLoop += Position;
        }
    }

    private void IntensityAndRange()
    {

        if (setNewTargets)
        {
            intensityDelta = (intensityOrigin + Random.Range(-intensityOffset, intensityOffset) - m_light.intensity) * speed;


            rangeTarget = rangeOrigin + Random.Range(-rangeOffset, rangeOffset);
            rangeDelta = (rangeTarget - m_light.range) * speed;

            setNewTargets = false;
        }

        m_light.intensity += intensityDelta;
        m_light.range += rangeDelta;


        if (Mathf.Abs(m_light.range - rangeTarget) < 5f * scale) 
            setNewTargets = true;
     }

    private void Position()
    {
        if (setNewTargets)
        {
            positionDelta = (positionOrigin + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized * positionOffset - transform.position) * speed;
            
        }
        transform.position += positionDelta;
    }

    void Update()
    {
        if (deltaSum >= 0.02f)
        { 
            m_MainLoop();
            deltaSum -= 0.02f;
        }
        deltaSum += Time.deltaTime;
	}
}



#if UNITY_EDITOR

[CustomEditor(typeof(FlickeringLight))]
public class MyScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var flicker = target as FlickeringLight;

        flicker.MakeSourceStationary = GUILayout.Toggle(flicker.MakeSourceStationary, "Make Source Stationary");

      if (!flicker.MakeSourceStationary)
            flicker.positionOffset = EditorGUILayout.FloatField(new GUIContent("Position Offset Multiplier:", "Source's movement freedom. Increasing this value will increase shadow flickering. IF you don't want shadow flickering, check 'Make Source Stationary'."), flicker.positionOffset);

    }
}

#endif
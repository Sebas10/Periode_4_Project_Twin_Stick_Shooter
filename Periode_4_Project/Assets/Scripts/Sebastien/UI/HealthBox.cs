﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBox : HealthPlayer
{
    [SerializeField] private float m_AmountHealth;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(m_Slider.value != 100)
            {
                TakeHeal(m_AmountHealth);
                Destroy(gameObject);
            }            
        }
    }
}

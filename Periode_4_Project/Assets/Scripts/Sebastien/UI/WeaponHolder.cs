﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHolder : MonoBehaviour
{
    [SerializeField] private GameObject[] m_WeaponSlots;

    [SerializeField] private PlayerController m_PlayerController;

    public bool Handgun = false, SecondWeapon = false;

    private void Awake()
    {
        m_WeaponSlots[0].SetActive(true);
        m_WeaponSlots[1].SetActive(false);
        m_WeaponSlots[2].SetActive(false);
    }

    private void Update()
    {
        if (Handgun)
        {
            m_PlayerController.GetComponent<PlayerController>().m_AssaultRifle = false;
            m_WeaponSlots[0].SetActive(false);
            m_WeaponSlots[1].SetActive(true);
            m_WeaponSlots[2].SetActive(false);
        }
        if (Handgun && SecondWeapon)
        {
            m_PlayerController.GetComponent<PlayerController>().m_AssaultRifle = true;
            m_WeaponSlots[0].SetActive(false);
            m_WeaponSlots[1].SetActive(false);
            m_WeaponSlots[2].SetActive(true);
        }
    }
}

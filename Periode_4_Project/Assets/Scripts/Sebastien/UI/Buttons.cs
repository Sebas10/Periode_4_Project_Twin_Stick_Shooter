﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{

    [SerializeField]
    private int m_Level;

    public void PlayGame()
    {
        SceneManager.LoadScene(m_Level);
    }
   
}

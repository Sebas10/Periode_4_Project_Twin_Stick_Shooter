﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour
{
    [SerializeField]
    private string m_LevelName;

    public void Loadlevel()
    {
        SceneManager.LoadScene(m_LevelName);
    }

}

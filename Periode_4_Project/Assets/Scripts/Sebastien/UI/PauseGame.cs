﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    [SerializeField]
    private GameObject[] m_ObjectsToPause = new GameObject[2];//Put player and weapon objects in this aray
    [SerializeField]
    private GameObject[] m_EnemyPauseObjects = new GameObject[7];//Put all enemy's in aray that have the AI component
    [SerializeField]
    private GameObject m_PauseScreen, UI_In_Game;

    public bool m_PauseB = false;

    private void Awake()
    {
        UI_In_Game.SetActive(true);
        m_PauseScreen.SetActive(false);
    }

    void Update()
    {
        //Pause game with button P/esc and reset it if it's pressed again
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            m_PauseB = !m_PauseB;
        }

        if (m_PauseB)
        {
            Pause();
        }
        else
        {
            Continue();
        }

    }


    /// <summary>
    /// Pause all GameObjects that need to pause if paused screen is on
    /// </summary>
    private void Pause()
    {
        //Sets Bitmap on
        m_PauseScreen.SetActive(true);

        UI_In_Game.SetActive(false);

        for (int i = 0; i < m_EnemyPauseObjects.Length; i++)
        {
            if(m_EnemyPauseObjects[i] != null)
            {
                m_EnemyPauseObjects[i].GetComponent<AI>().m_AmIPaused = true;
            }            
        }
        if(m_ObjectsToPause[0] != null)
        {
            m_ObjectsToPause[0].GetComponent<Shooting_Script>().m_AmIPaused = true;
        }       
        m_ObjectsToPause[1].GetComponent<PlayerController>().m_AmIPaused = true;
    }

    /// <summary>
    /// Sets all GameObjects on in play mode. Paused GameObjects sets false
    /// </summary>
    private void Continue()
    {
        m_PauseScreen.SetActive(false);

        UI_In_Game.SetActive(true);

        for (int i = 0; i < m_EnemyPauseObjects.Length; i++)
        {
            if (m_EnemyPauseObjects[i] != null)
            {
                m_EnemyPauseObjects[i].GetComponent<AI>().m_AmIPaused = false;
            }
        }
        if (m_ObjectsToPause[0] != null)
        {

        }
        m_ObjectsToPause[1].GetComponent<PlayerController>().m_AmIPaused = false;
    }

    public void ContinueButton()
    {
        m_PauseB = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    [SerializeField]
    private Text m_ScoreText;
    private int m_Score;

    private void Update()
    {
        m_ScoreText.text = m_Score.ToString();
    }

    public void ScorePlus(int amountPoints)
    {
        m_Score += amountPoints;
    }
}

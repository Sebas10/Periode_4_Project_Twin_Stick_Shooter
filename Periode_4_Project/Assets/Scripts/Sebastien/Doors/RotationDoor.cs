﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationDoor : MonoBehaviour
{
    [SerializeField]
    private Transform m_Origin;

    private bool m_MayOpenDoor = false;

    [SerializeField]
    private float m_Degree;

    void Update()
    {
        
        if (m_MayOpenDoor)
        {
            RotateDoor();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            m_MayOpenDoor = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            m_MayOpenDoor = false;
        }
    }

    /// <summary>
    /// Rotate the door from origin rotation to rotation that you put in
    /// </summary>
    private void RotateDoor()
    {
        Mathf.LerpAngle(m_Origin.rotation.y, m_Degree, 5f * Time.deltaTime);
        m_Origin.rotation = Quaternion.Lerp(m_Origin.rotation, Quaternion.Euler(0, m_Degree, 0), 2f * Time.deltaTime);
    }
}

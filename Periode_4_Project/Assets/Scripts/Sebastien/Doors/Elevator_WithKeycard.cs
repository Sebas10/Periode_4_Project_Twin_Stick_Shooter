﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator_WithKeycard : MonoBehaviour
{
    private bool m_Move = false;

    
    [SerializeField]
    private Transform target;//Target that doors need to move to

    [SerializeField]
    private Transform[] m_ElevatorDoors = new Transform[2];//Doors that you want to move towards the target

    public bool m_KeyCardOn = false;

    [SerializeField]
    private float speed;//Speed that the doors move

    [SerializeField] private GameObject m_ElevatorLight;//Light that turns in to green when you open the doors
    [SerializeField] private Material m_MaterialLight;//Material of the elevator light that changes in to green when you open the doors

    private void Start()
    {
        //Changes the material it's emmision color in to red in the beginning
        m_MaterialLight = m_ElevatorLight.GetComponent<Renderer>().material;
        m_MaterialLight.SetColor("_EmissionColor", Color.red);
    }

    void Update()
    {
        float step = speed * Time.deltaTime;
        if (m_Move)
        {
            m_ElevatorDoors[0].position = Vector3.Lerp(m_ElevatorDoors[0].position, target.position, step);
            m_ElevatorDoors[1].position = Vector3.Lerp(m_ElevatorDoors[1].position, target.position, step);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.F) && m_KeyCardOn)
        {
            //Changes the material it's emmision color in to green in the beginning
            m_MaterialLight.SetColor("_EmissionColor", Color.green);
            StartCoroutine(MoveDoors());
        }
    }

    private IEnumerator MoveDoors()
    {        
        yield return new WaitForSeconds(1);
        m_Move = true;
    }
}

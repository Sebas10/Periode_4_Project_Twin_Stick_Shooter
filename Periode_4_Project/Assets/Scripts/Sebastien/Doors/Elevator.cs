﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{

    private bool m_Move = false;

    [SerializeField]
    private Transform target;

    [SerializeField]
    private Transform[] m_ElevatorDoors = new Transform[2];

    [SerializeField]
    private float speed;

    [SerializeField] private GameObject m_Light;
    [SerializeField] private Material m_MaterialLight;

    private void Start()
    {
        m_MaterialLight = m_Light.GetComponent<Renderer>().material;
        m_MaterialLight.SetColor("_EmissionColor", Color.red);
        StartCoroutine(MoveDoors());
    }

    void Update()
    {
        float step = speed * Time.deltaTime;
        if (m_Move)
        {
            m_ElevatorDoors[0].position = Vector3.Lerp(m_ElevatorDoors[0].position, target.position, step);
            m_ElevatorDoors[1].position = Vector3.Lerp(m_ElevatorDoors[1].position, target.position, step);
        }
    }


    private IEnumerator MoveDoors()
    {
        yield return new WaitForSeconds(2);
        m_MaterialLight.color = Color.green;
        m_MaterialLight.SetColor("_EmissionColor", Color.green);
        m_Move = true;
    }
}

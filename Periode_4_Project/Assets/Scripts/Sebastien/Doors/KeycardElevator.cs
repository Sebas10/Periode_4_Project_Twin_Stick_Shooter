﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeycardElevator : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Elevator;

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Destroy(gameObject);
                m_Elevator.GetComponent<Elevator_WithKeycard>().m_KeyCardOn = true;
                
            }
            
        }
    }
}

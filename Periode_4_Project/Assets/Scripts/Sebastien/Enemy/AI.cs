﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(NavMeshAgent))]
public class AI : MonoBehaviour
{
    [SerializeField]
    private Transform m_Target, Enemy, m_BarrelEnd;

    [SerializeField]
    private Rigidbody m_rbd;

    [SerializeField]
    private NavMeshAgent m_Agent;

    [SerializeField]
    private Rigidbody m_Projectile;

    public bool m_AmIPaused = false;

    [SerializeField]
    private float SpeedProjectile = 1500f;
    private RaycastHit m_raycastHit;

    private HealthPlayer m_HP;

    private int m_Time = 10;

    private void Start()
    {
        m_rbd.GetComponent<Rigidbody>();
    }


    /// <summary>
    /// Distance between object himself and the target
    /// </summary>
    private Vector3 Distance
    {
        get
        {
            return (m_Target.position - transform.position).normalized;
        }
    }

    void Update()
    {
        m_rbd.constraints = RigidbodyConstraints.FreezeRotationZ;
        if (m_AmIPaused)//if the game is not paused it does nothing
            return;

        if (m_Time >= 60)//time for the next bullet for the enemy
        {
            StartCoroutine(LaunchFireBall());
            m_Time = 0;
        }

        if (Physics.Raycast(transform.position, Distance, out m_raycastHit, 15f))
        {

            if (m_raycastHit.collider.name == "Player")//goes to the last place where the enemy has hit the player with his raycast
            {
                m_Agent.SetDestination(m_Target.position);
                m_Time++;
                Enemy.LookAt(m_Target);

                
            }


            Debug.DrawLine(transform.position, m_raycastHit.point, Color.green);
        }
        else
        {
            Debug.DrawRay(transform.position, Distance * 15f, Color.red);
            m_Agent.SetDestination(transform.position);//blijft stil staan als hij niks ziet
        }
        
    }

    private IEnumerator LaunchFireBall()
    {
        Rigidbody clone = Instantiate(m_Projectile, m_BarrelEnd.position, Enemy.rotation);
        clone.AddForce(transform.forward * SpeedProjectile);
        yield return new WaitForSeconds(0f);
    }
}

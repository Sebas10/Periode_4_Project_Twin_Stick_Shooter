﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthEnemy : MonoBehaviour
{
    [SerializeField]
    private int m_HP = 100;

    [SerializeField]
    private GameObject Tekst;

    private void Update()
    {
        if (m_HP <= 0)
        {
            Tekst.GetComponent<Score>().ScorePlus(100);
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Current health - the amount of damage that is done
    /// </summary>
    /// <param name="amountDamage">amount of damage that deals the object</param>
    public void TakeDamage(int amountDamage)
    {
        m_HP -= amountDamage;        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            m_Player.GetComponent<HealthPlayer>().m_ArmorOn = true;
            Destroy(gameObject);
        }
    }
}

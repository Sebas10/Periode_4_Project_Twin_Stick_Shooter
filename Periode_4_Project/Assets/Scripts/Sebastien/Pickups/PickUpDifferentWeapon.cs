﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpDifferentWeapon : MonoBehaviour
{
    [SerializeField] private Shooting_Script m_Shooting_Script;
    [SerializeField] private WeaponHolder m_WeaponHolder;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            m_WeaponHolder.GetComponent<WeaponHolder>().SecondWeapon = true;
            m_Shooting_Script.GetComponent<Shooting_Script>().m_TypeWeapon = "Auto";
            Destroy(gameObject);
        }
    }
}

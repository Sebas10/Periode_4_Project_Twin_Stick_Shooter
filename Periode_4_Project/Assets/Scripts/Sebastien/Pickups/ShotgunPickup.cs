﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunPickup : MonoBehaviour
{
    [SerializeField] private Shooting_Script m_Shooting_Script;
    [SerializeField] private WeaponHolder m_WH;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            m_WH.GetComponent<WeaponHolder>().SecondWeapon = true;
            m_Shooting_Script.GetComponent<Shooting_Script>().m_TypeWeapon = "Shotgun";
            Destroy(gameObject);
        }
    }
}

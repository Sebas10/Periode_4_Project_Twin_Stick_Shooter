﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpWeapon : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Gun, m_Ammo, m_AmmoText;

    [SerializeField]
    private WeaponHolder m_WH;

    private void Awake()
    {        
        m_Gun.SetActive(false);
        m_AmmoText.SetActive(false);        
    }    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            m_WH.GetComponent<WeaponHolder>().Handgun = true;
            m_AmmoText.SetActive(true);
            m_Gun.SetActive(true);
            Destroy(gameObject);
            Destroy(m_Ammo.gameObject);
        }
    }

}

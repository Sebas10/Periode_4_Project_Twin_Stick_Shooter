﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Shooting_Script : MonoBehaviour
{
    [SerializeField] private GameObject[] m_Weapons;

    [SerializeField] private Transform m_BarrelEnd;

    [SerializeField] private Rigidbody m_Projectile;

    [SerializeField] private Transform m_PlayerParent;
    private float Stray = 5;
    private float NextShot;

    [SerializeField]
    protected int m_MaxAmunition;
    private int m_CurrentAmunition;
    private float m_ReloadTime = 2f;
    private bool isReloading = false;

    private RaycastHit m_raycastHit;

    [SerializeField]
    private Text m_AmmoText;

    private int[] m_GetalWapen = new int[5];

    private bool m_MagSchieten = true;

    [SerializeField] private float m_Projectilespeed;

    public bool m_AmIPaused = false;

    [SerializeField]
    public string m_TypeWeapon;

    private void Start()
    {
        m_Weapons[0].SetActive(true);
        m_Weapons[1].SetActive(false);
        m_Weapons[2].SetActive(false);

        m_CurrentAmunition = m_MaxAmunition;
    }

    private void Update()
    {
        if (m_AmIPaused)
            return;

        Debug.DrawLine(m_BarrelEnd.position, m_BarrelEnd.position + m_BarrelEnd.forward, Color.green);
        m_AmmoText.text = "Ammo : " + m_CurrentAmunition.ToString() + "/" + m_MaxAmunition.ToString();

        if (isReloading)
            return;
        if (m_CurrentAmunition <= 0)
        {
            m_CurrentAmunition = 0;
            StartCoroutine(Reload());
            return;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(Reload());
        }

        if (m_TypeWeapon == "HandGun")
        {
            m_Weapons[0].SetActive(true);
            m_Weapons[1].SetActive(false);
            m_Weapons[2].SetActive(false);

            m_MaxAmunition = 8;
            if (Input.GetMouseButtonDown(0) && m_MagSchieten)
            {
                FireHandGun();
                m_PlayerParent.Rotate(m_PlayerParent.rotation.x, 0, m_PlayerParent.rotation.z);
                if (Physics.Raycast(m_BarrelEnd.position, m_BarrelEnd.forward, out m_raycastHit, 10f))
                {
                    if (m_raycastHit.collider.tag == "Enemy")
                    {
                        m_raycastHit.transform.gameObject.GetComponent<HealthEnemy>().TakeDamage(25);
                    }
                }
            }
        }//Number 0 in array of m_Weapons

        if (m_TypeWeapon == "Shotgun")
        {
            m_Weapons[0].SetActive(false);
            m_Weapons[1].SetActive(true);
            m_Weapons[2].SetActive(false);

            m_MaxAmunition = 3;
            if (Input.GetMouseButton(0) && m_MagSchieten)
            {
                FireShotgun();
                m_PlayerParent.Rotate(m_PlayerParent.rotation.x, 0, m_PlayerParent.rotation.z);
                if (Physics.Raycast(m_BarrelEnd.position, -m_BarrelEnd.forward, out m_raycastHit, 15f))
                {
                    if (m_raycastHit.collider.tag == "Enemy")
                    {
                        m_raycastHit.transform.gameObject.GetComponent<HealthEnemy>().TakeDamage(50);
                    }
                }
            }
        }//Number 1 in array of m_Weapons

        if (m_TypeWeapon == "Auto")
        {
            m_Weapons[0].SetActive(false);
            m_Weapons[1].SetActive(false);
            m_Weapons[2].SetActive(true);
            m_MaxAmunition = 15;
            if (Input.GetMouseButton(0) && m_MagSchieten)
            {
                FireAutoGun();
                m_PlayerParent.Rotate(m_PlayerParent.rotation.x, 0, m_PlayerParent.rotation.z);
                if (Physics.Raycast(m_BarrelEnd.position, -m_BarrelEnd.forward, out m_raycastHit, 15))
                {
                    if (m_raycastHit.collider.tag == "Enemy")
                    {

                        m_raycastHit.transform.gameObject.GetComponent<HealthEnemy>().TakeDamage(5);
                    }
                }
            }

        }//Number 2 in array of m_Weapons
    }


    /// <summary>
    /// The time to wait before you can shoot again
    /// </summary>
    /// <returns></returns>
    IEnumerator Reload()
    {
        isReloading = true;
        yield return new WaitForSeconds(m_ReloadTime);
        m_CurrentAmunition = m_MaxAmunition;
        isReloading = false;
    }

    private void FireHandGun()
    {
        m_CurrentAmunition -= 1;
        Rigidbody instantiatedProjectile = Instantiate(m_Projectile, m_BarrelEnd.position, Quaternion.identity) as Rigidbody;
        instantiatedProjectile.AddForce(m_BarrelEnd.forward * m_Projectilespeed);
        StartCoroutine(DestroyBullets(instantiatedProjectile.gameObject));
    }

    private void FireShotgun()
    {
        if (Time.time > NextShot)
        {
            m_CurrentAmunition -= 1;
            var randomNumberX = Random.Range(-Stray, Stray);
            var randomNumberY = Random.Range(-Stray, Stray);
            Rigidbody instantiatedProjectile = Instantiate(m_Projectile, m_BarrelEnd.position, Quaternion.identity) as Rigidbody;
            m_BarrelEnd.Rotate(randomNumberX, randomNumberY, 0.0f);
            instantiatedProjectile.AddForce(-m_BarrelEnd.forward * m_Projectilespeed);
            NextShot = Time.time + 0.1f;
            StartCoroutine(DestroyBullets(instantiatedProjectile.gameObject));
        }
    }

    private void FireAutoGun()
    {
        if (Time.time > NextShot)
        {
            m_CurrentAmunition -= 1;
            Rigidbody instantiatedProjectile = Instantiate(m_Projectile, m_BarrelEnd.position, Quaternion.identity) as Rigidbody;
            instantiatedProjectile.AddForce(-m_BarrelEnd.forward * m_Projectilespeed);
            NextShot = Time.time + 0.1f;
            StartCoroutine(DestroyBullets(instantiatedProjectile.gameObject));
        }
    }

    IEnumerator DestroyBullets(GameObject bullet)
    {
        yield return new WaitForSeconds(2);
        Destroy(bullet.gameObject);
    }
}

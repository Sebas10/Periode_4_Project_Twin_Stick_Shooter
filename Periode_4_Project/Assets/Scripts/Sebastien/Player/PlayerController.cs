﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Camera m_Cam;

    private Vector3 m_MousePos;

    [SerializeField] private Transform m_PlayerParent, PlayerBody;


    private float m_Speed = 5f;

    private Vector3 m_PlayerPos;
    [SerializeField] private Rigidbody rbody;

    public bool m_AmIPaused = false;

    private Ray rayForward;

    private RaycastHit rayHitForward;

    private GameObject rayHitForwardObject;
    private string rayHitString;

    protected Vector3 dir;

    public bool m_AssaultRifle = false;

    private void Start()
    {
        rbody = GetComponent<Rigidbody>();
        rayForward = new Ray(m_PlayerParent.position, m_PlayerParent.forward);
    }

    private void Update()
    {
        //As long the object is not paused it does not return
        if (m_AmIPaused)
            return;

        m_MousePos = Input.mousePosition;
        Ray ray = m_Cam.ScreenPointToRay(m_MousePos);
        RaycastHit hitray;
        if (Physics.Raycast(ray, out hitray))
        {
            dir = (PlayerBody.position - hitray.point).normalized;

            dir.y = 0;

            PlayerBody.rotation = Quaternion.LookRotation(-dir);
        }


    }

    private void FixedUpdate()
    {
        if (m_AmIPaused)
            return;
        Movement();

        rbody.MovePosition(new Vector3(m_PlayerParent.position.x, transform.position.y, m_PlayerParent.position.z));
    }

    /// <summary>
    /// Moves the player with W,A,S,D,ctrl,leftShift
    /// </summary>
    private void Movement()
    {
        if (Input.GetKey(KeyCode.W))
        {
            m_PlayerParent.position = new Vector3(m_PlayerParent.position.x, m_PlayerParent.position.y, m_PlayerParent.position.z + m_Speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            m_PlayerParent.position = new Vector3(m_PlayerParent.position.x - m_Speed * Time.deltaTime, m_PlayerParent.position.y, m_PlayerParent.position.z);
        }
        if (Input.GetKey(KeyCode.D))
        {
            m_PlayerParent.position = new Vector3(m_PlayerParent.position.x + m_Speed * Time.deltaTime, m_PlayerParent.position.y, m_PlayerParent.position.z);
        }
        if (Input.GetKey(KeyCode.S))
        {
            m_PlayerParent.position = new Vector3(m_PlayerParent.position.x, m_PlayerParent.position.y, m_PlayerParent.position.z - m_Speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            m_Speed = 7f;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.LeftControl))
        {
            m_Speed = 5f;
        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            m_Speed = 3f;
        }
    }
}
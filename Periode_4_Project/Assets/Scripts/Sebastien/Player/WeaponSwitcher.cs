﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Weapon
{
    nothing = 0,
    handgun,
    shotgun
}

public class WeaponSwitcher : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> m_Weapons;

    private int m_CurrentWeapon;
    
    void Start()
    {
        m_Weapons = new List<GameObject>();
    }

    void Update()
    {
        Scroll();        
    }

    IEnumerator SwitchWeapon()
    {
        yield return new WaitForSeconds(0.5f);
    }

    public void AddWeapon(GameObject weapon)
    {
        m_Weapons.Add(weapon);
    }

    private void Scroll()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            m_CurrentWeapon += 1;
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            m_CurrentWeapon -= 1;
        }

    }
}

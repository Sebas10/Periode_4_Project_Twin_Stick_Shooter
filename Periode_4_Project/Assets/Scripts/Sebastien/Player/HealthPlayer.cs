﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthPlayer : MonoBehaviour
{
    public Slider m_Slider;

    public bool m_ArmorOn = false;

    private int m_Timer;

    [SerializeField]
    private DeathScreen Death;

    [SerializeField]
    private GameObject[] m_ArmorImages = new GameObject[2];

    private void Start()
    {
        m_ArmorImages[0].SetActive(true);
        m_ArmorImages[1].SetActive(false);
        m_Slider.value = 100f;
        m_Timer = 0;        
    }

    private void Update()
    {
        //If armor is ON then the player can't take any damage
        if (m_ArmorOn)
        {
            m_ArmorImages[0].SetActive(false);
            m_ArmorImages[1].SetActive(true);
            m_Timer++;
            if (m_Timer > 600)
            {
                m_ArmorOn = false;
                m_ArmorImages[0].SetActive(true);
                m_ArmorImages[1].SetActive(false);
                m_Timer = 0;
            }
        }

        if(m_Slider.value <= 0)
        {
            Dead();
        }
    }   

    /// <summary>
    /// Health player - amount of damage
    /// </summary>
    /// <param name="amountDamage">amount of damage that object deals to the player</param>
    public void TakeDamage(float amountDamage)
    {
        if (m_ArmorOn)
            return;
        m_Slider.value -= amountDamage;
    }

    /// <summary>
    /// Current Health Player + the amount of health that he gets of a object
    /// </summary>
    /// <param name="amountHealth"> amount of health that regenerates the player</param>
    public void TakeHeal(float amountHealth)
    {
        m_Slider.value += amountHealth;
    }

    private void Dead()
    {
        Death.PlayerDeath();
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Projectile")
        {
            TakeDamage(25f);
            Destroy(col.gameObject);
        }
    }
}
